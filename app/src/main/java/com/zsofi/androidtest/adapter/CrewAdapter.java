package com.zsofi.androidtest.adapter;

/**
 * Created by false on 2017. 03. 28..
 */


import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zsofi.androidtest.R;
import com.zsofi.androidtest.models.Cast;
import com.zsofi.androidtest.network.MovieDbManager;

import java.util.List;

public class CrewAdapter extends ArrayAdapter<Cast> {

    public CrewAdapter(Context context, List<Cast> castList) {
        super(context, 0, castList);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Cast cast = super.getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.cast_item_layout, parent, false);
        }
        ImageView castImage = (ImageView) convertView.findViewById(R.id.castImage);
        TextView castName = (TextView) convertView.findViewById(R.id.castName);
        TextView castCharacter = (TextView) convertView.findViewById(R.id.castCharacter);
        Picasso.with(getContext())
                .load(MovieDbManager.IMAGE_BASE_URL + cast.getProfilePath())
                .into(castImage);
        castName.setText(String.valueOf(cast.getName()));
        castCharacter.setText(String.valueOf(cast.getCharacter()));
        return convertView;
    }
}
