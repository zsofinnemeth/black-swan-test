package com.zsofi.androidtest.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zsofi.androidtest.R;
import com.zsofi.androidtest.activity.DetailedPageActivity;
import com.zsofi.androidtest.models.Movie;
import com.zsofi.androidtest.network.MovieDbManager;

import java.util.List;

/**
 * Created by false on 2017. 02. 28..
 */

public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.ViewHolder> {

    private List<Movie> movieItems;
    private Context context;

    public MovieListAdapter(List<Movie> movieItems, Context context) {
        this.movieItems = movieItems;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
       final Movie movieItem = movieItems.get(position);

        Picasso.with(context)
                .load(MovieDbManager.IMAGE_BASE_URL + movieItem.getPosterPath())
                .into(holder.moviePoster);

        holder.textViewTitle.setText(movieItem.getTitle());
        holder.textViewYear.setText(movieItem.getReleaseDateText());
        holder.textViewVoteAverage.setText(Float.toString(movieItem.getAverageVote()));
        holder.textViewOverview.setText(movieItem.getOverview());
        holder.buttonMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailedPageActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("movie", movieItem);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return movieItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textViewTitle, textViewYear, textViewVoteAverage, textViewOverview;
        private ImageView moviePoster;
        private Button buttonMore;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewTitle = (TextView) itemView.findViewById(R.id.textViewTitle);
            textViewYear = (TextView) itemView.findViewById(R.id.textViewYear);
            textViewVoteAverage = (TextView) itemView.findViewById(R.id.textViewVoteAverage);
            textViewOverview = (TextView) itemView.findViewById(R.id.textViewOverview);
            moviePoster = (ImageView) itemView.findViewById(R.id.moviePoster);
            buttonMore = (Button) itemView.findViewById(R.id.buttonMore);
        }
    }
}

