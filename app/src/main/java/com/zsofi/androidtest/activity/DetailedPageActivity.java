package com.zsofi.androidtest.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zsofi.androidtest.R;
import com.zsofi.androidtest.adapter.CrewAdapter;
import com.zsofi.androidtest.models.Cast;
import com.zsofi.androidtest.models.CreditsResponse;
import com.zsofi.androidtest.models.Movie;
import com.zsofi.androidtest.network.MovieDbManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by false on 2017. 03. 01..
 */

public class DetailedPageActivity extends AppCompatActivity {


    ImageView poster;
    TextView title;
    TextView year;
    TextView description;
    TextView directorName;
    ListView castList;
    CrewAdapter adapter;
    Movie movie;
    List<Cast> cast;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailed_page);
        movie = (Movie) getIntent().getSerializableExtra("movie");
        poster = (ImageView) findViewById(R.id.movieDetailPoster);
        title = (TextView) findViewById(R.id.movieDetailTitle);
        year = (TextView) findViewById(R.id.movieDetailYear);
        description = (TextView) findViewById(R.id.movieDetailDescriptionBody);
        directorName = (TextView) findViewById(R.id.movieDetailDirector);
        castList = (ListView) findViewById(R.id.movieDetailCast);
        Picasso.with(getApplicationContext())
                .load(MovieDbManager.BACKDROP_BASE_URL + movie.getBackdropPath())
                .into(poster);
        title.setText(movie.getTitle());
        year.setText("(" + movie.getReleaseDateText().substring(0,4) + ")");
        description.setText(movie.getOverview());

        MovieDbManager.getInstance().loadCredits(movie.getId(), new Callback<CreditsResponse>() {
            @Override
            public void onResponse(Call<CreditsResponse> call, Response<CreditsResponse> response) {
                directorName.setText(response.body().getDirector().getName());
                cast = response.body().getCast();
                adapter = new CrewAdapter(getApplicationContext(),cast.subList(0,5));
                castList.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<CreditsResponse> call, Throwable t) {

            }
        });

        poster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                poster.setVisibility(View.GONE);
            }
        });
    }
}
