package com.zsofi.androidtest.network;

import com.zsofi.androidtest.models.CreditsResponse;
import com.zsofi.androidtest.models.GenresListResponse;
import com.zsofi.androidtest.models.LoadPopularMoviesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieDbApi {

    @GET("movie/popular")
    Call<LoadPopularMoviesResponse> getPopularMovies(@Query("page") int page);

    @GET("genre/movie/list")
    Call<GenresListResponse> getGenres();

    @GET("movie/{movie_id}/credits")
    Call<CreditsResponse> getCredits(@Path("movie_id") int movieId);

}

